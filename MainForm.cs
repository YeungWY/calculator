﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class MainForm : Form
    {
        private double Number1 = 0;
        private bool OperatorJustSet = false;
        private string Operator = "";
        private double Number2 = 0;
        private double Memory = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonOne_Click(object sender, EventArgs e)
        {
            NumberPressed("1");
        }

        private void NumberPressed(string MyNumber)
        {
            // 1 2 + 2
            // 1 + 2 = 

            if (label2.Text == "0" || OperatorJustSet)
            {
                label2.Text = "";
                OperatorJustSet = false;
            }

            label2.Text += MyNumber;

            if (Operator == "")
            {
                Number1 = double.Parse(label2.Text);
            }
            else
            {
                Number2 = double.Parse(label2.Text);
            }
        }


        private void buttonPlus_Click(object sender, EventArgs e)
        {
            if (Operator.Length > 0 && Number1 != 0 && Number2 !=0)
            {
                buttonEqual_Click(sender,e);
            }
            Operator = "+";
            OperatorJustSet = true;
            label1.Text = Number1.ToString() + Operator;
        }

        private void buttonTwo_Click(object sender, EventArgs e)
        {
            NumberPressed("2");
        }

        private void buttonThree_Click(object sender, EventArgs e)
        {
            NumberPressed("3");            
        }

        private void buttonFour_Click(object sender, EventArgs e)
        {
            NumberPressed("4");
        }

        private void buttonFive_Click(object sender, EventArgs e)
        {
            NumberPressed("5");
        }

        private void buttonSix_Click(object sender, EventArgs e)
        {
            NumberPressed("6");
        }

        private void buttonSeven_Click(object sender, EventArgs e)
        {
            NumberPressed("7");
        }

        private void buttonEight_Click(object sender, EventArgs e)
        {
            NumberPressed("8");
        }

        private void buttonNine_Click(object sender, EventArgs e)
        {
            NumberPressed("9");
        }

        private void buttonZero_Click(object sender, EventArgs e)
        {
            NumberPressed("0");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            Operator = "-";
            OperatorJustSet = true;
            label1.Text = Number1.ToString() + Operator;
        }

        private void buttonMultiplication_Click(object sender, EventArgs e)
        {
            Operator = "*";
            OperatorJustSet = true;
            label1.Text = Number1.ToString() + Operator;
        }

        private void buttonDivision_Click(object sender, EventArgs e)
        {
            Operator = "/";
            OperatorJustSet = true;
            label1.Text = Number1.ToString() + Operator;
        }

        private void buttonPlusMinus_Click(object sender, EventArgs e)
        {
            if(Operator == "")
            {
                Number1 = Number1 * -1;
                label2.Text = Number1.ToString();
            }
            else
            {
                Number2 = Number2 * -1;
                label2.Text = Number2.ToString();
            }
            
            
        }

        private void buttonCancel_Click(object sender, EventArgs e)         
        {
            ClearEquation();

            OperatorJustSet = false;
        }

        private void ClearEquation()
        {
            Number1 = 0;
            Number2 = 0;
            Operator = "";
            label1.Text = "";
            label2.Text = "0";
        }

        private void buttonEqual_Click(object sender, EventArgs e)       
        {           
            
            if (Operator == "+")                                         
            {
                if (OperatorJustSet == true)
                {
                    Number2 = double.Parse(label2.Text);
                    OperatorJustSet = false;
                }
                
                var NewNumber = Number1 + Number2;

                ClearEquation();
                
                label2.Text = NewNumber.ToString();
                Number1 = NewNumber;
                OperatorJustSet = true;
            }
                
            else if (Operator == "-")
            {
                if (OperatorJustSet == true)
                {
                    Number2 = double.Parse(label2.Text);
                    OperatorJustSet = false;
                }
                
                var NewNumber = Number1 - Number2;
                ClearEquation();
                label2.Text = NewNumber.ToString();
                Number1 = NewNumber;
                OperatorJustSet = true;
            }

            else if (Operator == "*")
            {
                if (OperatorJustSet == true)
                {
                    Number2 = double.Parse(label2.Text);
                    OperatorJustSet = true;
                }
                var NewNumber = Number1 * Number2;
                ClearEquation();
                label2.Text = NewNumber.ToString();
                Number1 = NewNumber;
                OperatorJustSet = true;
            }
                
            else if (Operator == "/")
            {
                if (OperatorJustSet == true)
                {
                    Number2 = double.Parse(label2.Text);
                    OperatorJustSet = true;
                }
                var NewNumber = Number1 / Number2;
                ClearEquation();
                label2.Text = NewNumber.ToString();
                Number1 = NewNumber;
                OperatorJustSet = true;
            }
            

        }

        private void buttonPercentage_Click(object sender, EventArgs e)
        {
            Operator = "%";
            var NewNumber = Number1 * Number2 * 0.01;
            label1.Text = Number1.ToString() + "*" + NewNumber.ToString();
            label2.Text = NewNumber.ToString();
        }

        private void buttonSquareroot_Click(object sender, EventArgs e)
        {
            Operator = "√";
            var NewNumber = Math.Sqrt(Number1);
            label1.Text = "sqrt(" + Number1.ToString() + ")";
            label2.Text = NewNumber.ToString();


        }

        private void buttonReciprocal_Click(object sender, EventArgs e)
        {
            Operator = "1/x";
            var NewNumber = 1 / Number1;
            label1.Text = NewNumber.ToString();
            label2.Text = NewNumber.ToString();                     
        }

        private void buttonPoint_Click(object sender, EventArgs e)        
        {
            if (label2.Text.IndexOf(".") < 0)
                                                      
                label2.Text += ".";     
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            Number2 = 0;
            label2.Text = Number2.ToString();
        }

        private void buttonBackSpace_Click(object sender, EventArgs e)
        {
            label2.Text = label2.Text.Substring(0, label2.Text.Length - 1);
        }

        private void buttonMplus_Click(object sender, EventArgs e)
        {
            Memory += double.Parse(label2.Text);
            label3.Text = "M";
        }

        private void buttonMminus_Click(object sender, EventArgs e)
        {
            Memory -= double.Parse(label2.Text);
            label3.Text = "M";
        }

        private void buttonMS_Click(object sender, EventArgs e)
        {
            Memory = double.Parse(label2.Text);
            label3.Text = "M";
        }

        private void buttonMR_Click(object sender, EventArgs e)
        {
            label2.Text = Memory.ToString();
        }

        private void buttonMC_Click(object sender, EventArgs e)
        {
            Memory = 0;
            label3.Text = "";
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '0':
                case '.':
                    NumberPressed(e.KeyChar.ToString());
                    e.Handled = true;
                    break;
                case '+':
                    buttonPlus_Click(null, null);
                    e.Handled = true;
                    break;
                case '-':
                    buttonMinus_Click(null, null);
                    e.Handled = true;
                    break;
                case '*':
                    buttonMultiplication_Click(null, null);
                    e.Handled = true;
                    break;
                case '/':
                    buttonDivision_Click(null, null);
                    e.Handled = true;
                    break;
                case '√':
                    buttonSquareroot_Click(null, null);
                    e.Handled = true;
                    break;
                case '%':
                    buttonPercentage_Click(null, null);
                    e.Handled = true;
                    break;
                case '=':
                    buttonEqual_Click(null, null);
                    e.Handled = true;
                    break;
                
                default:
                    return;
            }
            Console.WriteLine(e.KeyChar.ToString());
            

        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine(e.KeyCode.ToString());
        }

    }
}
            
